package unite_go

import (
	"gitee.com/hzsuoyi/unite-go/channels"
	"gitee.com/hzsuoyi/unite-go/mp"
	"gitee.com/hzsuoyi/unite-go/open"
	"gitee.com/hzsuoyi/unite-go/uni"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/worker"
)

func NewClient(tokenMgr unite.TokenMgr) unite.CommonClient {
	cc := &unite.Client{
		TokenMgr: tokenMgr,
	}
	//fmt.Println("初始化", tokenMgr.GetConfig().AppId, tokenMgr.GetConfig().Platform)
	if tokenMgr.GetConfig().Platform == unite.AppPlatformWorker {
		return worker.NewClient(cc)
	} else if tokenMgr.GetConfig().Platform == unite.AppPlatformOpenWechat {
		return open.NewClient(cc)
	} else if tokenMgr.GetConfig().Platform == unite.AppPlatformChannel {
		return channels.NewClient(cc)
	} else if tokenMgr.GetConfig().Platform.IsUni() {
		return uni.NewClient(cc)
	} else {
		return mp.NewClient(cc)
	}
}
