package uni

import (
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"testing"
)

func TestDecData(t *testing.T) {
	u := new(UserInfo)
	cc := NewClient(&unite.Client{
		TokenMgr: unite.NewWxTokenMgrByConfig(unite.Config{
			Platform: unite.AppPlatformBaidu,
		}),
	})
	if err := cc.DecData("aef18e96e2c017e9abc15de3050396c6", &EncData{
		EncryptedData: "nZgktVvwVYTKQjG34G1HJFN4768k0CRYDWI/Tw1/7i3ZVQIJhjwCMk5zxFkXT4Ewjtqjy5/TZHps02lUxnBVkM+XjJ17bux1RFYdGSJCSi25P9ah1nXQx99aXzs4hTRZdq1fM1mXkmpRWWpr1bgL4KEvdQ60By37BlWrFFarhpcX9hk8GIvnuME3xADMu+KVdT8Apr8Q76mO+SfLWac+q6RyXKVaFThH1LvsUuaqU9Z/P9jmYVaJDB2iPpjFcNk976jnJxsGnIKX1SpAi/0bTjhQtG4uOGyBBZV5n/Ng2Pw=",
		Iv:            "aef18e96e2c017e9abc15Q==",
	}, u); err != nil {
		fmt.Println(err)
	}
	fmt.Println(u)
}
