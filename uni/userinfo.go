package uni

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"gitee.com/xiaochengtech/alipay"
	"net/http"
)

type UserInfo struct {
	OpenId     string `json:"openId"`
	NickName   string `json:"nickName"`
	Gender     int64  `json:"gender"`
	Sex        int64  `json:"sex"` // for baidu
	City       string `json:"city"`
	Province   string `json:"province"`
	Country    string `json:"country"`
	AvatarUrl  string `json:"avatarUrl"`
	HeadImgUrl string `json:"headimgurl"` // for baidu
	UnionId    string `json:"unionId"`
	Watermark  struct {
		AppId     string `json:"appId"`
		Timestamp int64  `json:"timestamp"`
	} `json:"watermark"`
}

type PhoneNumber struct {
	PhoneNumber     string `json:"phoneNumber"`
	PurePhoneNumber string `json:"purePhoneNumber"`
	CountryCode     string `json:"countryCode"`
	WaterMark       struct {
		AppId     string `json:"appid"`
		TimeStamp int64  `json:"timestamp"`
	} `json:"watermark"`
}

// 前端传入的用户信息
type EncData struct {
	//RawData       string `json:"rawData"`
	//Signature     string `json:"signature"`
	EncryptedData string `json:"encryptedData"`
	Iv            string `json:"iv"`
}

// GetSession 需要使用到的结构体
type AuthResp struct {
	uutil.ErrResp
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionId    string `json:"unionid"`
}

var jsCode2SessionURL = unite.ClientURL{
	WX:    "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code",
	TT:    "https://developer.toutiao.com/api/apps/jscode2session?appid=%s&secret=%s&code=%s",
	Baidu: "https://spapi.baidu.com/oauth/jscode2sessionkey?client_id=%s&sk=%s&code=%s",
}

// 小程序拿code换openid
// 暂时不支持头条小程序参数 anonymousCode
func (client *Client) GetSession(code string) (authResp AuthResp, err error) {
	c := client.GetConfig()
	if c.Platform == unite.AppPlatformAlipay {
		return client.getSessionAlipay(code)
	}

	u := jsCode2SessionURL.Format(c.Platform, c.GetAppId(), c.AppSecret, code)

	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &authResp)
	return
}

// 支付宝小程序比较特别，目前引入第三方
func (client *Client) getSessionAlipay(code string) (authResp AuthResp, err error) {
	var aliRsp alipay.SystemOauthTokenResponse
	if aliRsp, err = client.getClientAlipay().SystemOauthToken(alipay.SystemOauthTokenBody{
		GrantType: "authorization_code",
		Code:      code,
	}); err != nil {
		return
	}
	authResp = AuthResp{
		OpenId:     aliRsp.Data.UserId,
		SessionKey: aliRsp.Data.AccessToken,
	}
	return
}

// 解密前端传入的小程序用户信息
func (client *Client) DecData(sessionKey string, info *EncData, decData interface{}) (err error) {
	if info == nil {
		err = fmt.Errorf("encData is nil")
		return
	}
	aesKey, err := base64.StdEncoding.DecodeString(sessionKey)
	if err != nil {
		return
	}
	cipherText, err := base64.StdEncoding.DecodeString(info.EncryptedData)
	if err != nil {
		return
	} else if len(cipherText)%8 != 0 {
		err = errors.New("cipherText cannot exact division by 8")
		return
	}

	block, err := aes.NewCipher(aesKey)
	if err != nil {
		return
	}
	iv, err := base64.StdEncoding.DecodeString(info.Iv)
	if err != nil {
		return
	}
	if len(iv) == 0 || len(iv)%8 != 0 {
		err = fmt.Errorf("invalid IV length, iv=%s", info.Iv)
		return
	}
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)
	trailingPadding := uint(cipherText[len(cipherText)-1])
	if trailingPadding > 16 {
		err = errors.New("invalid trailing padding")
		return
	}
	data := cipherText[:len(cipherText)-int(trailingPadding)]
	if client.GetConfig().Platform == unite.AppPlatformBaidu {
		var contentLen int32
		_ = binary.Read(bytes.NewBuffer(data[16:20]), binary.BigEndian, &contentLen)
		data = data[20 : 20+contentLen]
	}

	if err = json.Unmarshal(data, decData); err != nil {
		return
	}
	if client.GetConfig().Platform == unite.AppPlatformBaidu {
		decData.(*UserInfo).AvatarUrl = decData.(*UserInfo).HeadImgUrl
		decData.(*UserInfo).Gender = decData.(*UserInfo).Sex
	}
	return
}
