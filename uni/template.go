package uni

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

const tplMsgURL = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=%s"
const subMsgURL = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=%s"

func NewTemplateMessage(openId, templateId, page, formId string, data unite.TemplateData) unite.WxaTemplate {
	return unite.WxaTemplate{
		Template: unite.Template{
			ToUser:     openId,
			TemplateId: templateId,
			Data:       data,
		},
		Page:   page,
		FormId: formId,
	}
}

// https://developers.weixin.qq.com/miniprogram/dev/api/open-api/template-message/sendTemplateMessage.html
func (client *Client) SendTemplateMessage(t unite.WxaTemplate) (err error) {
	u := fmt.Sprintf(tplMsgURL, client.GetToken())

	data, _ := json.Marshal(t)
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	err = uutil.DoRequestJson(req, nil)
	return
}

// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
func (client *Client) SendSubscribeMessage(t unite.WxaTemplate) (err error) {
	u := fmt.Sprintf(subMsgURL, client.GetToken())

	data, _ := json.Marshal(t)
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	err = uutil.DoRequestJson(req, nil)
	return
}
