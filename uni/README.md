# 小程序

## 已实现功能

- 模板消息
    - [x] 发送
- 消息管理
    - [x] 接受普通消息
    - [x] 接受事件推送
- 客服消息
    - [x] 发送文本
    - [x] 发送图片
- 二维码
    - [x] 临时小程序码
    - [x] 永久小程序码
- 素材管理
    - [x] 临时素材上传
    - [x] 获取临时素材
- 解码用户信息
    - [x] 用户信息
    - [x] 分享信息
    - [ ] 手机号
- 内容安全
    - [x] 图片
    - [x] 文本
- 动态消息
    - [x] 新建
    - [x] 更新
- 数据分析
