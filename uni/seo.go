package uni

import (
	"bytes"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

type (
	// Page 图文类
	Page struct {
		Path     string     `json:"path"`
		Query    string     `json:"query"`
		DataList []PageData `json:"data_list,omitempty"`
	}
	PageData struct {
		Type            string     `json:"@type"`
		Update          int64      `json:"update"` // 新增(覆盖)1 删除3
		ContentId       string     `json:"content_id"`
		PageType        int64      `json:"page_type"`   // 图文类2 问答类4
		CategoryId      int64      `json:"category_id"` // 内容类目
		Title           string     `json:"title"`
		Subtitle        []string   `json:"subtitle,omitempty"`
		Abstract        []string   `json:"abstract,omitempty"`
		Referer         string     `json:"referer"`
		CoverImg        []CoverImg `json:"cover_img,omitempty"`
		Tag             []string   `json:"tag,omitempty"`
		SearchWord      []string   `json:"searchword,omitempty"`
		TimePublish     int64      `json:"time_publish"`
		TimeModify      int64      `json:"time_modify"`
		PageAttribute   int64      `json:"page_attribute"` //问答类 0-普通问答；1-精准问答；默认填0
		MainBody        []MainBody `json:"mainbody"`
		Answer          Answer     `json:"answer"`
		SimilarQuestion []string   `json:"similar_question,omitempty"`
		Service         Service    `json:"service"`
		Author          Author     `json:"author,omitempty"`
	}

	Author struct {
		AuthorName string `json:"author_name"`
	}

	CoverImg struct {
		CoverImgUrl  string `json:"cover_img_url"`
		CoverImgSize int64  `json:"cover_img_size"`
	}

	MainBody struct {
		AnswerContent   string `json:"answer_content"`
		AnswerTimestamp int64  `json:"answer_timestamp"`
		AuthorName      string `json:"author_name"`
	}

	Answer struct {
		AnswerStyle int64    `json:"answer_style"`           //0-图文样式；1-短答案；2-长答案；3-步骤答案；默认填0
		ShortAnswer string   `json:"short_answer,omitempty"` // answer_style=1
		LongAnswer  string   `json:"long_answer,omitempty"`  // answer_style=2
		IntroAnswer string   `json:"intro_answer,omitempty"` // answer_style=3
		StepAnswer  []string `json:"step_answer,omitempty"`  // answer_style=3
	}

	Service struct {
		ServiceTitle    string `json:"service_title"`     //建议6个字以内，最多不超过8个字
		ServiceWeappId  string `json:"service_weapp_id"`  //
		ServiceWeappUrl string `json:"service_weapp_url"` //
	}
)

// SubmitPages 内容接入
// https://developers.weixin.qq.com/miniprogram/introduction/widget/we-search/WXAPAGES.html#%E4%B8%89%E3%80%81%E5%86%85%E5%AE%B9%E6%8E%A5%E5%85%A5%E6%96%B9%E5%BC%8F
func (client *Client) SubmitPages(pages []Page) error {
	b, _ := json.Marshal(struct {
		Pages []Page `json:"pages"`
	}{
		Pages: pages,
	})

	u := "https://api.weixin.qq.com/wxa/search/wxaapi_submitpages?access_token=" + client.GetToken()
	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader(b))
	return uutil.DoRequestJson(req, nil)
}
