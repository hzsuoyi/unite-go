package uni

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

type (
	TTText struct {
		Content string `json:"content"`
	}
	TTAuditTextRequest struct {
		Tasks []TTText `json:"tasks"`
	}
)

// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/sec-check/security.msgSecCheck.html
func (client *Client) MsgSecCheck(msg string) (err error) {
	if client.GetConfig().Platform.IsWechat() {
		return client.wxaTextAudit(msg)
	} else if client.GetConfig().Platform == unite.AppPlatformTT {
		return client.ttTextAudit(msg)
	} else {
		return
	}
}

func (client *Client) wxaTextAudit(msg string) (err error) {
	data, _ := json.Marshal(struct {
		Content string `json:"content"`
	}{
		Content: msg,
	})
	u := fmt.Sprintf("https://api.weixin.qq.com/wxa/msg_sec_check?access_token=%s", client.GetToken())
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	err = uutil.DoRequestJson(req, nil)
	return
}

func (client *Client) ttTextAudit(msg string) (err error) {
	data, _ := json.Marshal(TTAuditTextRequest{
		Tasks: []TTText{{msg}},
	})
	u := "https://developer.toutiao.com/api/v2/tags/text/antidirt"
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))
	req.Header.Add("X-Token", client.GetToken())
	var resp = new(TTAuditResponse)
	if err = uutil.DoRequestJson(req, resp); err != nil {
		return
	}
	if len(resp.Data) == 0 {
		return ErrAuditNoTask
	}
	if resp.Data[0].Code > 0 {
		return ErrAuditNoTask
	}

	for _, task := range resp.Data[0].Predicts {
		if task.Prob > 0.5 {
			return fmt.Errorf("not approve prob: %v, model: %v, target: %v", task.Prob, task.ModelName, task.Target)
		}
	}
	return
}
