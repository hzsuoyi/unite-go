package uni

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

type PhoneResp struct {
	uutil.ErrResp
	PhoneInfo PhoneNumber `json:"phone_info"`
}

// 获取手机号
// https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-info/phone-number/getPhoneNumber.html
func (client *Client) GetPhoneNumber(code string) (resp PhoneResp, err error) {
	u := fmt.Sprintf("https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=%s", client.GetToken())

	body := map[string]interface{}{
		"code": code,
	}
	bodyByte, _ := json.Marshal(body)
	req, _ := http.NewRequest("POST", u, bytes.NewBuffer(bodyByte))
	err = uutil.DoRequestJson(req, &resp)
	return
}
