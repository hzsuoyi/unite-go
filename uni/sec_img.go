package uni

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"image"
	"mime/multipart"
	"net/http"
	"net/textproto"
)

var (
	ErrAuditNoTask = errors.New("not found audit data")
)

type (
	TTImg struct {
		ImageData string `json:"image_data"`
	}
	TTAuditImgRequest struct {
		Targets []string `json:"targets"`
		Tasks   []TTImg  `json:"tasks"`
	}

	TTAuditResponseData struct {
		Code     int `json:"code"`
		Predicts []struct {
			Prob      float64 `json:"prob"`
			ModelName string  `json:"model_name"`
			Target    string  `json:"target"`
		}
	}
	TTAuditResponse struct {
		LogId string `json:"log_id"`
		Data  []TTAuditResponseData
	}
)

// TODO 调整图片尺寸
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/sec-check/security.imgSecCheck.html
func (client *Client) ImgAudit(media []byte) (err error) {
	if client.GetConfig().Platform.IsWechat() {
		return client.wxaImgAudit(media)
	} else if client.GetConfig().Platform == unite.AppPlatformTT {
		return client.ttImgAudit(media)
	} else {
		return
	}
}

func (client *Client) wxaImgAudit(media []byte) (err error) {
	b := new(bytes.Buffer)
	writer := multipart.NewWriter(b)

	var filename string
	var contentTyp string
	_, ext, _ := image.Decode(bytes.NewReader(media))
	filename = fmt.Sprintf("%s.%s", uutil.Md5(string(media)), ext)
	if ext == "jpeg" {
		contentTyp = "image/jpeg"
	} else if ext == "png" {
		contentTyp = "image/png"
	}

	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="media"; filename="%s"`, filename))
	h.Set("Content-Type", contentTyp)
	part, _ := writer.CreatePart(h)
	part.Write(media)

	writer.Close()

	u := fmt.Sprintf("https://api.weixin.qq.com/wxa/img_sec_check?access_token=%s", client.GetToken())
	req, _ := http.NewRequest("POST", u, b)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	err = uutil.DoRequestJson(req, nil)
	return
}

func (client *Client) ttImgAudit(media []byte) (err error) {
	data, _ := json.Marshal(TTAuditImgRequest{
		Targets: []string{"porn", "politics", "disgusting", "ad"},
		Tasks: []TTImg{
			{
				ImageData: base64.StdEncoding.EncodeToString(media),
			},
		},
	})
	u := "https://developer.toutiao.com/api/v2/tags/image/"
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))
	req.Header.Add("X-Token", client.GetToken())

	var resp = new(TTAuditResponse)
	if err = uutil.DoRequestJson(req, resp); err != nil {
		return
	}
	if len(resp.Data) == 0 {
		return ErrAuditNoTask
	}
	if resp.Data[0].Code > 0 {
		return ErrAuditNoTask
	}

	for _, task := range resp.Data[0].Predicts {
		if task.Prob > 0.5 {
			return fmt.Errorf("not approve prob: %v, model: %v, target: %v", task.Prob, task.ModelName, task.Target)
		}
	}
	return
}
