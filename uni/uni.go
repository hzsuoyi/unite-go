package uni

import (
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/xiaochengtech/alipay"
	"github.com/json-iterator/go"
)

// 禁止转义html
var json = jsoniter.ConfigFastest

//------------------------------------------------------------------------------

type Client struct {
	*unite.Client
}

// 初始化sdk
func NewClient(client *unite.Client) *Client {
	return &Client{
		Client: client,
	}
}

// 支付宝采用SDK
// https://gitee.com/xiaochengtech/alipay
func (client *Client) getClientAlipay() *alipay.Client {
	config := alipay.Config{
		AppId: client.GetConfig().AppId,
	}
	return alipay.NewClient(true, client.GetConfig().PublicKey, client.GetConfig().PrivateKey, config)
}
