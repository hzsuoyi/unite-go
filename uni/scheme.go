package uni

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

type SchemeResp struct {
	uutil.ErrResp
	OpenLink string `json:"openlink"`
}

// H5跳转小程序
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/url-scheme/urlscheme.generate.html
func (client *Client) Scheme(path, query string, expire int64) (resp SchemeResp, err error) {
	u := fmt.Sprintf("https://api.weixin.qq.com/wxa/generatescheme?access_token=%s", client.GetToken())

	body := map[string]interface{}{
		"jump_wxa": map[string]interface{}{
			"path":  path,
			"query": query,
		},
		"is_expire":   expire > 0,
		"expire_time": expire,
	}
	bodyByte, _ := json.Marshal(body)
	req, _ := http.NewRequest("POST", u, bytes.NewBuffer(bodyByte))
	err = uutil.DoRequestJson(req, &resp)
	return
}
