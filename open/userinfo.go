package open

import (
	"fmt"
	"gitee.com/hzsuoyi/unite-go/mp"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

// 开放平台获取access_token
// https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_Login/Development_Guide.html
func (client *Client) GetSession(code string) (authResp mp.OauthToken, err error) {
	c := client.GetConfig()
	u := fmt.Sprintf("https://api.weixin.qq.com/sns/oauth2/access_token?appid=%v&secret=%v&code=%v&grant_type=authorization_code", c.GetAppId(), c.AppSecret, code)

	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &authResp)
	return
}

// 获取用户信息
// https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_Login/Authorized_API_call_UnionID.html
func (client *Client) GetUserInfo(accessToken, openId string) (resp mp.UserInfo, err error) {
	u := fmt.Sprintf("https://api.weixin.qq.com/sns/userinfo?access_token=%v&openid=%v", accessToken, openId)

	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &resp)
	return
}
