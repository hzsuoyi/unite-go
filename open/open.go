package open

import (
	"gitee.com/hzsuoyi/unite-go/unite"
	jsoniter "github.com/json-iterator/go"
)

var json = jsoniter.ConfigFastest

type Client struct {
	*unite.Client
}

// 初始化sdk
func NewClient(client *unite.Client) *Client {
	return &Client{
		Client: client,
	}
}
