package worker

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
	"net/url"
)

type UpdateWorkerRequest struct {
	UserId          string          `json:"userid"`
	ExternalProfile ExternalProfile `json:"external_profile"`
}

type ExternalProfile struct {
	ExternalAttr []ExternalAttr `json:"external_attr"`
}

type ExternalAttr struct {
	Type int    `json:"type"` //属性类型: 0-文本 1-网页 2-小程序
	Name string `json:"name"` // 对应企业微信后台创建的名字
	Web  struct {
		Url   string `json:"url"`
		Title string `json:"title"`
	} `json:"web"`
	Text struct {
		Value string `json:"value"`
	} `json:"text"`
	MiniProgram unite.ReturnWXAppMsg `json:"miniprogram"`
}

type OauthUserInfo struct {
	uutil.ErrResp
	UserId         string `json:"UserId,omitempty"`
	DeviceId       string `json:"DeviceId,omitempty"`
	ExternalUserId string `json:"external_userid,omitempty"`
}

// 更新成员信息 https://work.weixin.qq.com/api/doc/90000/90135/90197
func (client *Client) UpdateUser(workerRequest UpdateWorkerRequest) (err error) {
	data, _ := json.Marshal(workerRequest)
	u := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader(data))
	err = uutil.DoRequestJson(req, nil)
	return
}

func (client *Client) GetOpenId(contactId string) (openId string, err error) {
	data, err := json.Marshal(map[string]string{
		"userid": contactId,
	})
	if err != nil {
		return
	}
	u := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader(data))
	body, err := uutil.DoRequest(req)
	if err != nil {
		return
	}
	openId = json.Get(body, "openid").ToString()
	return
}

// RedirectOauth 获取重定向地址
func (client *Client) RedirectOauth(callbackURL, state string) string {
	if state != "" {
		state = fmt.Sprintf("&state=%s", state)
	}
	return fmt.Sprintf("https://open.weixin.qq.com/connect/oauth2/authorize?appid=%v&redirect_uri=%v&response_type=code&scope=snsapi_base%v#wechat_redirect",
		client.GetConfig().GetAppId(), url.QueryEscape(callbackURL), state, // 这里就在里边做urlEncode了
	)
}

// GetUserInfoByOauth 通过 code 换取 用户信心
func (client *Client) GetUserInfoByOauth(code string) (token OauthUserInfo, err error) {
	u := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=%v&code=%v",
		client.GetToken(), code)
	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &token)
	return
}
