package worker

import (
	"bytes"
	"fmt"
	"net/http"

	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
)

type TemplateRequest struct {
	ChatType        string                `json:"chat_type"`       //single group
	ExternalUserId  []string              `json:"external_userid"` //single
	Sender          string                `json:"sender"`          //group
	WelcomeCode     string                `json:"welcome_code"`    //欢迎语
	Text            *unite.ReturnTextMsg  `json:"text,omitempty"`
	Image           *unite.ReturnImageMsg `json:"image,omitempty"`
	Link            *unite.ReturnLinkMsg  `json:"link,omitempty"`
	MiniProgramPage *unite.ReturnWXAppMsg `json:"miniprogram,omitempty"`
}
type TemplateResponse struct {
	FailList []string `json:"fail_list"`
	MsgId    string   `json:"msgid"`
}
type ContactMeRequest struct {
	Type        int      `json:"type"`  //联系方式类型,1-单人, 2-多人
	Scene       int      `json:"scene"` //场景，1-在小程序中联系，2-通过二维码联系
	Remark      string   `json:"remark"`
	SkipVerify  bool     `json:"skip_verify"`
	User        []string `json:"user"`
	Conclusions struct {
		Text            *unite.ReturnTextMsg  `json:"text,omitempty"`
		Image           *unite.ReturnImageMsg `json:"image,omitempty"`
		Link            *unite.ReturnLinkMsg  `json:"link,omitempty"`
		MiniProgramPage *unite.ReturnWXAppMsg `json:"miniprogram,omitempty"`
	} `json:"conclusions"`
}
type ContactMeResponse struct {
	ConfigId string `json:"config_id"`
	QrCode   string `json:"qr_code"`
}

// 群发消息任务 https://work.weixin.qq.com/api/doc/90000/90135/92135
// 根据客户id群发
func (client *Client) SendSingleTemplate(users []string, sender string, msg TemplateRequest) (res TemplateResponse, err error) {
	msg.ChatType = "single"
	msg.ExternalUserId = users
	msg.Sender = sender
	data, _ := json.Marshal(msg)
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_msg_template?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	err = uutil.DoRequestJson(req, &res)
	return
}

// 根据客服id群发
func (client *Client) SendGroupTemplate(sender string, msg TemplateRequest) (res TemplateResponse, err error) {
	msg.ChatType = "group"
	msg.Sender = sender
	data, _ := json.Marshal(msg)
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_msg_template?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	err = uutil.DoRequestJson(req, &res)
	return
}

// 发送欢迎语 https://work.weixin.qq.com/api/doc/90000/90135/92137
func (client *Client) SendWelcome(welcomeCode string, msg TemplateRequest) (err error) {
	msg.WelcomeCode = welcomeCode
	data, _ := json.Marshal(msg)
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/send_welcome_msg?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	err = uutil.DoRequestJson(req, nil)
	return
}

// 配置[联系我] https://work.weixin.qq.com/api/doc/90000/90135/92572
func (client *Client) DeployContactMe(contactMeRequest ContactMeRequest) (contactMeResponse *ContactMeResponse, err error) {
	data, _ := json.Marshal(contactMeRequest)
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_contact_way?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))

	contactMeResponse = new(ContactMeResponse)
	err = uutil.DoRequestJson(req, contactMeResponse)
	return
}

// 删除[联系我] https://work.weixin.qq.com/api/doc/90000/90135/92572
func (client *Client) DeleteContactMe(configId string) (err error) {
	data, _ := json.Marshal(map[string]string{
		"config_id": configId,
	})
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_contact_way?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	err = uutil.DoRequestJson(req, nil)
	return
}
