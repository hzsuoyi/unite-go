package worker

import (
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil/wxbizmsgcrypt"
	jsoniter "github.com/json-iterator/go"
	"net/http"
)

// 禁止转义html
var json = jsoniter.ConfigFastest

type Client struct {
	*unite.Client
	encoder *wxbizmsgcrypt.WXBizMsgCrypt
}

// 初始化sdk
func NewClient(client *unite.Client) *Client {
	c := client.GetConfig()
	return &Client{
		Client:  client,
		encoder: wxbizmsgcrypt.NewWXBizMsgCrypt(c.Token, c.AesKey, c.AppId, wxbizmsgcrypt.XmlType),
	}
}

func (client *Client) DecodeMessage(req *http.Request, b []byte) ([]byte, *wxbizmsgcrypt.CryptError) {
	q := req.URL.Query()
	return client.encoder.DecryptMsg(q.Get("msg_signature"), q.Get("timestamp"), q.Get("nonce"), b)
}
