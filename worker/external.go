package worker

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

// 客服人员ids
type UserList struct {
	FollowUser []string `json:"follow_user"`
}

// 客户ids
type ContactList struct {
	ContactId []string `json:"external_userid"`
}

// 客户详情
type Contact struct {
	ExternalContact ExternalContact `json:"external_contact"`
	FollowUser      []FollowUser    `json:"follow_user"`
}
type ExternalContact struct {
	ContactId string `json:"external_userid"`
	Name      string `json:"name"`
	Avatar    string `json:"avatar"`
	UnionId   string `json:"unionid"`
}
type FollowUser struct {
	Userid     string `json:"userid"`
	Remark     string `json:"remark"`
	Createtime int64  `json:"createtime"`
}

// 客户统计数据
type ContactData struct {
	BehaviorData []BehaviorData `json:"behavior_data"`
}
type BehaviorData struct {
	StatTime            int64   `json:"stat_time"`             //数据日期
	NewApplyCnt         int64   `json:"new_apply_cnt"`         //发起申请数(客服主动)
	NewContactCnt       int64   `json:"new_contact_cnt"`       //新增客户数
	ChatCnt             int64   `json:"chat_cnt"`              //聊天总数（客服主动）
	MessageCnt          int64   `json:"message_cnt"`           //发送消息数
	ReplyPercentage     float64 `json:"reply_percentage"`      //已回复聊天占比
	AvgReplyTime        int     `json:"avg_reply_time"`        //平均首次回复时长(min)
	NegativeFeedbackCnt int64   `json:"negative_feedback_cnt"` //删除/拉黑成员的客户数
}
type UpdateRemarkRequest struct {
	UserId         string `json:"userid"`
	ExternalUserid string `json:"external_userid"`
	Remark         string `json:"remark"`
	Description    string `json:"description"`
}

// 获取客服人员列表 https://work.weixin.qq.com/api/doc/90000/90135/92570
func (client *Client) GetServicePersonnelList() (userList UserList, err error) {
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_follow_user_list?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	err = uutil.DoRequestJson(req, &userList)
	return
}

// 获取客户列表 https://work.weixin.qq.com/api/doc/90000/90135/92113
func (client *Client) GetContactList(userId string) (contactList ContactList, err error) {
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/list?access_token=%s&userid=%s", client.GetToken(), userId)
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	err = uutil.DoRequestJson(req, &contactList)
	return
}

// 获取客户详情 https://work.weixin.qq.com/api/doc/90000/90135/92114
func (client *Client) GetContact(contactId string) (contact Contact, err error) {
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get?access_token=%s&external_userid=%s", client.GetToken(), contactId)
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	err = uutil.DoRequestJson(req, &contact)
	return
}

// 修改客户备注 https://work.weixin.qq.com/api/doc/90000/90135/92115
// TODO 使用结构体进行修改
func (client *Client) UpdateContactRemark(reqBody UpdateRemarkRequest) (err error) {
	data, err := json.Marshal(reqBody)
	if err != nil {
		return
	}
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	return uutil.DoRequestJson(req, nil)
}

// 获取统计数据 https://work.weixin.qq.com/api/doc/90000/90135/92132
func (client *Client) GetBehaviorData(ids []string, start, end int64) (contactData ContactData, err error) {
	reqBody := struct {
		UserId    []string `json:"userid"`
		StartTime int64    `json:"start_time"`
		EndTime   int64    `json:"end_time"`
	}{
		UserId:    ids,
		StartTime: start,
		EndTime:   end,
	}
	data, err := json.Marshal(reqBody)
	if err != nil {
		return
	}
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_user_behavior_data?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	err = uutil.DoRequestJson(req, &contactData)
	return
}

// 外部联系人openid转换
// https://work.weixin.qq.com/api/doc/90001/90143/92292
func (client *Client) GetExternalOpenId(contactId string) (openId string, err error) {
	reqBody := struct {
		ExternalUserid string `json:"external_userid"`
	}{
		ExternalUserid: contactId,
	}
	data, err := json.Marshal(reqBody)
	if err != nil {
		return
	}
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/convert_to_openid?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	body, err := uutil.DoRequest(req)
	if err != nil {
		return
	}
	openId = json.Get(body, "openid").ToString()
	return
}

// external_userid转换
func (client *Client) FromServiceExternalUserID(ext string, agent int) (openId string, err error) {
	data, err := json.Marshal(map[string]interface{}{
		"external_userid": ext,
		"source_agentid":  agent,
	})
	if err != nil {
		return
	}
	url := fmt.Sprintf("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/from_service_external_userid?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
	body, err := uutil.DoRequest(req)
	if err != nil {
		return
	}
	openId = json.Get(body, "external_userid").ToString()
	return
}
