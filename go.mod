module gitee.com/hzsuoyi/unite-go

go 1.14

require (
	gitee.com/xiaochengtech/alipay v0.0.0-20201102065921-6b5566eb7989
	github.com/go-redis/redis/v8 v8.8.2 // indirect
	github.com/json-iterator/go v1.1.11
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
)
