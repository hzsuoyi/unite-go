package uutil

import (
	"errors"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

type ErrResp struct {
	ErrCode int64
	ErrMsg  string
}

func (e ErrResp) Error() string {
	return fmt.Sprintf("errCode=%d, errMsg=%s", e.ErrCode, e.ErrMsg)
}

//------------------------------------------------------------------------------

// 只是单纯的去掉了proxy
var netTransport = &http.Transport{
	DialContext: (&net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
		DualStack: true,
	}).DialContext,
	MaxIdleConns:          100,
	IdleConnTimeout:       90 * time.Second,
	TLSHandshakeTimeout:   10 * time.Second,
	ExpectContinueTimeout: 1 * time.Second,
}

var netClient = &http.Client{
	Timeout:   time.Second * 10,
	Transport: netTransport,
}

var json = jsoniter.ConfigFastest

func GetHttpClient() *http.Client {
	return netClient
}

func DoRequest(req *http.Request) (rb []byte, err error) {
	resp, err := netClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = errors.New("http status ok")
		return
	}
	return ioutil.ReadAll(resp.Body)
}

func DoRequestJson(req *http.Request, v interface{}) (err error) {
	rb, err := DoRequest(req)
	if err != nil {
		return
	}
	// 先反序列化到resp
	var resp ErrResp
	resp.ErrCode = jsoniter.Get(rb, "errcode").ToInt64()
	if resp.ErrCode > 0 {
		resp.ErrMsg = jsoniter.Get(rb, "errmsg").ToString()
		return resp
	}
	resp.ErrCode = jsoniter.Get(rb, "error").ToInt64() // 为了兼容百度的错误码
	if resp.ErrCode > 0 {
		resp.ErrMsg = jsoniter.Get(rb, "error_description").ToString()
		return resp
	}

	if v == nil {
		return
	}
	err = json.Unmarshal(rb, v)
	if err != nil {
		err = fmt.Errorf("parse json err=%s, body=%s", err.Error(), string(rb))
	}
	return
}
