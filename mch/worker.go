package mch

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

func (client *Client) NewRedPack(billNO, sender, openId, wishing, activity, remark, appId, ip string, amount int64) WorkRedPack {
	return WorkRedPack{
		NonceStr:    CDATA{uutil.GetRndString(16)},
		MchId:       CDATA{client.MchId},
		MchBillNO:   CDATA{billNO},
		WxAppId:     CDATA{appId},
		SendName:    CDATA{sender},
		ReOpenId:    CDATA{openId},
		TotalAmount: CDATA{fmt.Sprint(amount)},
		TotalNum:    CDATA{"1"},
		Wishing:     CDATA{wishing},
		ActName:     CDATA{activity},
		Remark:      CDATA{remark},
		ClientIp:    CDATA{ip},
	}
}

// TODO 企业现金红包旧接口
// 几个关于企业支付相关的问题
// 无法触达用户 https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_4&index=3
// 新接口，计算企业签名，要的是固定字段 https://developers.weixin.qq.com/community/develop/doc/00006223a64f4870b0893d38f51c00?_at=1578193958936
func (client *Client) SendRedPack(redPack WorkRedPack) error {
	// 微信支付签名
	redPack.Sign = CDATA{Text: client.SignXml(redPack)}

	data, err := xml.Marshal(redPack)
	if err != nil {
		return err
	}
	//fmt.Println(string(data))
	req, _ := http.NewRequest(http.MethodPost, "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack", bytes.NewReader(data))
	var res = new(WorkRedPackCallback)
	rb, err := client.DoRequest(req)
	if err != nil {
		return err
	}
	if err = xml.Unmarshal(rb, res); err != nil {
		return err
	} else if !res.Valid() {
		return res
	}
	return nil
}
