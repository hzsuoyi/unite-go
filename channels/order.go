package channels

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

const (
	orderListGetUrl   = "https://api.weixin.qq.com/channels/ec/order/list/get?access_token=%v" // 获取订单列表
	orderGetDetailUrl = "https://api.weixin.qq.com/channels/ec/order/get?access_token=%v"      // 获取订单详情

	OrderStatus10  = 10  //	待付款
	OrderStatus20  = 20  //	待发货（包括部分发货）
	OrderStatus21  = 21  //	部分发货
	OrderStatus30  = 30  //	待收货（包括部分发货）
	OrderStatus100 = 100 //	完成
	OrderStatus250 = 250 //	订单取消（包括未付款取消，售后取消等）
)

type (
	GetOrderListArgs struct {
		CreateTimeRange *TimeRange `json:"create_time_range,omitempty"` // 时间范围至少填一个
		UpdateTimeRange *TimeRange `json:"update_time_range,omitempty"`
		Status          int        `json:"status,omitempty"`
		OpenID          string     `json:"openid,omitempty"`
		PageSize        int        `json:"page_size,omitempty"`
		NextKey         string     `json:"next_key,omitempty"`
	}

	TimeRange struct {
		StartTime int64 `json:"start_time,omitempty"`
		EndTime   int64 `json:"end_time,omitempty"`
	}

	GetOrderListResp struct {
		OrderIDList []string `json:"order_id_list,omitempty"`
		NextKey     string   `json:"next_key,omitempty"`
		HasMore     bool     `json:"has_more,omitempty"`
	}

	GetOrderDetailArgs struct {
		OrderID             string `json:"order_id,omitempty"`
		EncodeSensitiveInfo bool   `json:"encode_sensitive_info,omitempty"`
	}

	GetOrderDetailResp struct {
		Order Order `json:"order,omitempty"`
	}

	Order struct {
		CreateTime      int64           `json:"create_time,omitempty,omitempty"`
		UpdateTime      int64           `json:"update_time,omitempty,omitempty"`
		OrderID         string          `json:"order_id,omitempty,omitempty"`
		Status          int             `json:"status,omitempty,omitempty"`
		OpenID          string          `json:"openid,omitempty,omitempty"`
		UnionID         string          `json:"unionid,omitempty,omitempty"`
		OrderDetail     OrderDetail     `json:"order_detail,omitempty"`
		AfterSaleDetail AfterSaleDetail `json:"aftersale_detail,omitempty"`
	}

	AfterSaleDetail struct {
		AfterSaleOrderList  []AfterSaleOrderInfo `json:"aftersale_order_list,omitempty"`
		OnAfterSaleOrderCnt int                  `json:"on_aftersale_order_cnt,omitempty"`
	}

	AfterSaleOrderInfo struct {
		AfterSaleOrderID string `json:"aftersale_order_id,omitempty"`
		Status           int    `json:"status,omitempty"`
	}

	OrderDetail struct {
		ProductInfos    []ProductInfo    `json:"product_infos,omitempty"`
		PriceInfo       PriceInfo        `json:"price_info,omitempty"`
		PayInfo         PayInfo          `json:"pay_info,omitempty"`
		DeliveryInfo    DeliveryInfo     `json:"delivery_info,omitempty"`
		CouponInfo      CouponInfo       `json:"coupon_info,omitempty"`
		ExtInfo         ExtInfo          `json:"ext_info,omitempty"`
		CommissionInfos []CommissionInfo `json:"commission_infos,omitempty"`
		SharerInfo      SharerInfo       `json:"sharer_info,omitempty"`
		SettleInfo      SettleInfo       `json:"settle_info,omitempty"`
		SkuSharerInfos  []SkuSharerInfo  `json:"sku_sharer_infos,omitempty"`
	}

	ProductInfo struct {
		ProductID             string              `json:"product_id,omitempty"`
		SkuID                 string              `json:"sku_id,omitempty"`
		ThumbImg              string              `json:"thumb_img,omitempty"`
		SkuCnt                int                 `json:"sku_cnt,omitempty"`
		SalePrice             int                 `json:"sale_price,omitempty"` // 分
		Title                 string              `json:"title,omitempty"`
		OnAfterSaleSkuCnt     int                 `json:"on_aftersale_sku_cnt,omitempty"`
		FinishAfterSaleSkuCnt int                 `json:"finish_aftersale_sku_cnt,omitempty"`
		SkuCode               string              `json:"sku_code,omitempty"`
		MarketPrice           int                 `json:"market_price,omitempty"` // 分
		SkuAttrs              []AttrInfo          `json:"sku_attrs,omitempty"`
		RealPrice             int                 `json:"real_price,omitempty"`
		OutProductID          string              `json:"out_product_id,omitempty"`
		OutSkuID              string              `json:"out_sku_id,omitempty"`
		IsDiscounted          bool                `json:"is_discounted,omitempty"`
		EstimatePrice         int                 `json:"estimate_price,omitempty"`
		IsChangePrice         bool                `json:"is_change_price,omitempty"`
		ChangePrice           int                 `json:"change_price,omitempty"`
		OutWarehouseID        string              `json:"out_warehouse_id,omitempty"`
		SkuDeliverInfo        SkuDeliverInfo      `json:"sku_deliver_info,omitempty"`
		ExtraService          ProductExtraService `json:"extra_service,omitempty"`
		UseDeduction          bool                `json:"use_deduction,omitempty"`
		DeductionPrice        int                 `json:"deduction_price,omitempty"`
	}

	SkuDeliverInfo struct {
		StockType           int   `json:"stock_type,omitempty"`
		PredictDeliveryTime int64 `json:"predict_delivery_time,omitempty"`
	}

	ProductExtraService struct {
		SevenDayReturn   int `json:"seven_day_return,omitempty"`
		FreightInsurance int `json:"freight_insurance,omitempty"`
	}

	AttrInfo struct {
		AttrKey   string `json:"attr_key,omitempty"`
		AttrValue string `json:"attr_value,omitempty"`
	}

	PayInfo struct {
		PrepayID      string `json:"prepay_id,omitempty"`
		PrepayTime    int64  `json:"prepay_time,omitempty"`
		PayTime       int64  `json:"pay_time,omitempty"`
		TransactionID string `json:"transaction_id,omitempty"`
		PaymentMethod int    `json:"payment_method,omitempty"`
	}

	PriceInfo struct {
		ProductPrice         int  `json:"product_price,omitempty"`
		OrderPrice           int  `json:"order_price,omitempty"`
		Freight              int  `json:"freight,omitempty"`
		DiscountedPrice      int  `json:"discounted_price,omitempty"`
		IsDiscounted         bool `json:"is_discounted,omitempty"`
		OriginalOrderPrice   int  `json:"original_order_price,omitempty"`
		EstimateProductPrice int  `json:"estimate_product_price,omitempty"`
		ChangeDownPrice      int  `json:"change_down_price,omitempty"`
		ChangeFreight        int  `json:"change_freight,omitempty"`
		IsChangeFreight      bool `json:"is_change_freight,omitempty"`
		UseDeduction         bool `json:"use_deduction,omitempty"`
		DeductionPrice       int  `json:"deduction_price,omitempty"`
	}

	DeliveryInfo struct {
		AddressInfo         AddressInfo           `json:"address_info,omitempty"`
		DeliveryProductInfo []DeliveryProductInfo `json:"delivery_product_info,omitempty"`
		ShipDoneTime        int64                 `json:"ship_done_time,omitempty"`
		DeliverMethod       int                   `json:"deliver_method,omitempty"`
		AddressUnderReview  AddressInfo           `json:"address_under_review,omitempty"`
		AddressApplyTime    int64                 `json:"address_apply_time,omitempty"`
		EwaybillOrderCode   string                `json:"ewaybill_order_code,omitempty"`
	}

	DeliveryProductInfo struct {
		WaybillID       string               `json:"waybill_id,omitempty"`
		DeliveryID      string               `json:"delivery_id,omitempty"`
		ProductInfos    []FreightProductInfo `json:"product_infos,omitempty"`
		DeliveryName    string               `json:"delivery_name,omitempty"`
		DeliveryTime    int64                `json:"delivery_time,omitempty"`
		DeliverType     int                  `json:"deliver_type,omitempty"`
		DeliveryAddress AddressInfo          `json:"delivery_address,omitempty"`
	}

	FreightProductInfo struct {
		ProductID  string `json:"product_id,omitempty"`
		SkuID      string `json:"sku_id,omitempty"`
		ProductCnt int    `json:"product_cnt,omitempty"`
	}

	AddressInfo struct {
		UserName              string           `json:"user_name,omitempty"`
		PostalCode            string           `json:"postal_code,omitempty"`
		ProvinceName          string           `json:"province_name,omitempty"`
		CityName              string           `json:"city_name,omitempty"`
		CountyName            string           `json:"county_name,omitempty"`
		DetailInfo            string           `json:"detail_info,omitempty"`
		TelNumber             string           `json:"tel_number,omitempty"`
		HouseNumber           string           `json:"house_number,omitempty"`
		VirtualOrderTelNumber string           `json:"virtual_order_tel_number,omitempty"`
		TelNumberExtInfo      TelNumberExtInfo `json:"tel_number_ext_info,omitempty"`
		UseTelNumber          int              `json:"use_tel_number,omitempty"`
		HashCode              string           `json:"hash_code,omitempty"`
	}

	TelNumberExtInfo struct {
		RealTelNumber        string `json:"real_tel_number,omitempty"`         //脱敏手机号
		VirtualTelNumber     string `json:"virtual_tel_number,omitempty"`      //完整的虚拟号码
		VirtualTelExpireTime int64  `json:"virtual_tel_expire_time,omitempty"` //主动兑换的虚拟号码过期时间
		GetVirtualTelCnt     int    `json:"get_virtual_tel_cnt,omitempty"`     //主动兑换虚拟号码次数
	}

	CouponInfo struct {
		UserCouponID string `json:"user_coupon_id,omitempty"`
	}

	ExtInfo struct {
		CustomerNotes string `json:"customer_notes,omitempty"` //用户备注
		MerchantNotes string `json:"merchant_notes,omitempty"` //商家备注
	}

	CommissionInfo struct {
		SkuID        string `json:"sku_id,omitempty"`       //商品skuid
		Nickname     string `json:"nickname,omitempty"`     //分账方昵称
		Typ          int    `json:"type,omitempty"`         //分账方类型，0：达人，1：团长
		Status       int    `json:"status,omitempty"`       //分账状态， 1：未结算，2：已结算
		Amount       int    `json:"amount,omitempty"`       //分账金额
		FinderID     string `json:"finder_id,omitempty"`    //达人视频号id
		OpenfinderID string `json:"openfinderid,omitempty"` //达人openfinderid
	}

	SharerInfo struct {
		SharerOpenid  string `json:"sharer_openid,omitempty"`  //分享员openid
		SharerUnionid string `json:"sharer_unionid,omitempty"` //分享员unionid
		SharerType    int    `json:"sharer_type,omitempty"`    //分享员类型，0：普通分享员，1：店铺分享员
		ShareScene    int    `json:"share_scene,omitempty"`    //分享场景，枚举值见ShareScene
	}

	SkuSharerInfo struct {
		SharerOpenid  string `json:"sharer_openid,omitempty"`  //分享员openid
		SharerUnionid string `json:"sharer_unionid,omitempty"` //分享员unionid
		SharerType    int    `json:"sharer_type,omitempty"`    //分享员类型，0：普通分享员，1：店铺分享员
		ShareScene    int    `json:"share_scene,omitempty"`    //分享场景，枚举值见ShareScene
		SkuID         string `json:"sku_id,omitempty"`         //商品skuid
		FromWecom     bool   `json:"from_wecom,omitempty"`     //是否来自企微分享
	}

	SettleInfo struct {
		PredictCommissionFee int `json:"predict_commission_fee,omitempty"` //预计技术服务费（单位为分）
		CommissionFee        int `json:"commission_fee,omitempty"`         //实际技术服务费（单位为分）（未结算时本字段为空）
	}
)

// 获取订单列表
// https://developers.weixin.qq.com/doc/channels/API/order/list_get.html
func (client *Client) GetOrderList(args GetOrderListArgs) (resp GetOrderListResp, err error) {
	u := fmt.Sprintf(orderListGetUrl, client.GetToken())

	data, _ := json.Marshal(args)
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	// 尝试修复偶现的EOF
	// https://studygolang.com/articles/09190
	req.Close = true
	err = uutil.DoRequestJson(req, &resp)
	return
}

// 获取订单详情
// https://developers.weixin.qq.com/doc/channels/API/order/get.html
func (client *Client) GetOrder(args GetOrderDetailArgs) (resp GetOrderDetailResp, err error) {
	u := fmt.Sprintf(orderGetDetailUrl, client.GetToken())

	data, _ := json.Marshal(args)
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	err = uutil.DoRequestJson(req, &resp)
	return
}
