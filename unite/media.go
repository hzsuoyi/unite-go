package unite

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/textproto"
)

// 上传媒体文件返回的结果
type UploadMediaResponse struct {
	uutil.ErrResp
	Type    string `json:"type"`
	MediaId string `json:"media_id"`
	//CreatedAt int64  `json:"created_at"`  FIXME 兼容企业微信
}

type MediaTyp string

const MediaTypImage MediaTyp = "image"
const MediaTypVoice MediaTyp = "voice"
const MediaTypVideo MediaTyp = "video"
const MediaTypThumb MediaTyp = "thumb"

var GetImageURL = ClientURL{
	WX:     "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s",
	Worker: "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s",
}

// UploadMedia 上传临时素材
func (c *Client) UploadMedia(mediaTyp MediaTyp, media []byte) (mediaId string, err error) {
	b := new(bytes.Buffer)
	writer := multipart.NewWriter(b)

	var filename string
	var contentTyp string
	if mediaTyp == MediaTypImage {
		_, ext, _ := image.Decode(bytes.NewReader(media))
		filename = fmt.Sprintf("%s.%s", uutil.Md5(string(media)), ext)
		if ext == "jpeg" {
			contentTyp = "image/jpeg"
		} else if ext == "png" {
			contentTyp = "image/png"
		}
	}

	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="media"; filename="%s"`, filename))
	h.Set("Content-Type", contentTyp)
	part, _ := writer.CreatePart(h)
	part.Write(media)

	writer.Close()
	u := GetImageURL.Format(c.GetConfig().Platform, c.GetToken(), mediaTyp)
	req, _ := http.NewRequest("POST", u, b)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	respJson := new(UploadMediaResponse)
	if err = uutil.DoRequestJson(req, respJson); err != nil {
		return
	}
	mediaId = respJson.MediaId
	return
}

// UploadMediaFile 上传本地文件，同时进行缓存下
func (c *Client) UploadMediaFile(mediaTyp MediaTyp, filePath string) (mediaId string, err error) {
	cacheKey := fmt.Sprintf("wx:media:%s:%s", c.TokenMgr.GetConfig().Platform, uutil.Md5(filePath))
	if mediaId = c.Get(cacheKey); len(mediaId) > 0 {
		return
	}

	f, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}
	// 如果没有的话，那么上传一下
	mediaId, err = c.UploadMedia(mediaTyp, f)

	_ = c.Set(cacheKey, mediaId, 257400)
	return
}

// UploadMediaURL 上传本地文件，同时进行缓存下
func (c *Client) UploadMediaURL(mediaTyp MediaTyp, url string) (mediaId string, err error) {
	cacheKey := fmt.Sprintf("wx:media:%s:%s", c.TokenMgr.GetConfig().Platform, uutil.Md5(url))
	if mediaId = c.Get(cacheKey); len(mediaId) > 0 {
		return
	}

	req, _ := http.NewRequest(http.MethodGet, url, nil)
	f, err := uutil.DoRequest(req)
	if err != nil {
		return
	}
	// 如果没有的话，那么上传一下
	mediaId, err = c.UploadMedia(mediaTyp, f)

	_ = c.Set(cacheKey, mediaId, 257400)
	return
}

// GetMedia 下载临时素材
func (c *Client) GetMedia(mediaId string) (media []byte, err error) {
	req, _ := http.NewRequest("GET", fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s", c.GetToken(), mediaId), nil)
	media, err = uutil.DoRequest(req)
	if bytes.Index(media, []byte("{")) == 0 {
		resp := new(uutil.ErrResp)
		if err = json.Unmarshal(media, resp); err == nil && resp.ErrCode != 0 {
			err = errors.New(resp.Error())
		}
	}
	return
}
