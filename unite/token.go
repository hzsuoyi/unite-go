package unite

import (
	"context"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"github.com/go-redis/redis/v8"
	"net/http"
	"sync/atomic"
	"time"
)

// GetToken 获取access token
func (c *Client) GetToken() string {
	token, isNew, err := c.TokenMgr.GetOrNewToken()
	if err != nil {
		fmt.Printf("wx-auth accessToken appId=%s, err=%s\n", c.GetConfig().AppId, err.Error())
		return ""
	}
	if isNew {
		//fmt.Printf("wx-auth accessToken, appId=%s, token=%s\n", c.GetConfig().AppId, token)
	}
	return token
}

type TokenMgr interface {
	GetConfig() Config                                    // 获取配置
	Set(k, v string, expire int64) error                  // 手动设置session
	Get(k string) string                                  // 获取session
	GetOrNewToken() (token string, isNew bool, err error) // 获取accessToken
	// TODO 实现一个接口次数管理器
}

const KeyToken = "token"
const KeyJSApi = "jsApi"
const KeyJsCard = "jsCard"

var tokenURL = ClientURL{
	WX:     "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s",
	TT:     "https://developer.toutiao.com/api/apps/token?grant_type=client_credential&appid=%s&secret=%s",
	Baidu:  "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s&scope=smartapp_snsapi_base",
	Worker: "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s",
}

type Token struct {
	Token   string
	Expires int64
}

func (token Token) IsNotExpire() bool {
	return token.Expires > time.Now().Unix()
}

// 传回access_token，有效期提前10分钟
func fetchToken(appId, appSecret string, platform AppPlatform) (token Token, err error) {
	if platform == AppPlatformAlipay {
		return Token{Expires: -1}, nil
	}
	req, _ := http.NewRequest("GET", tokenURL.Format(platform, appId, appSecret), nil)
	r := new(struct {
		uutil.ErrResp
		AccessToken string `json:"access_token"`
		ExpiresIn   int64  `json:"expires_in"`
	})
	if err = uutil.DoRequestJson(req, r); err != nil {
		return
	}

	return Token{
		Token:   r.AccessToken,
		Expires: r.ExpiresIn - 600, // 提前10分钟过期
	}, nil
}

//------------------------------------------------------------------------------

// WxTokenMgr 将 access_token 维护在内存中的基础版
type WxTokenMgr struct {
	Config
	session map[string]atomic.Value
}

func NewWxTokenMgr(appId, appSecret string) *WxTokenMgr {
	return &WxTokenMgr{
		Config: Config{
			AppId:     appId,
			AppSecret: appSecret,
		},
		session: make(map[string]atomic.Value),
	}
}

func NewWxTokenMgrByConfig(c Config) *WxTokenMgr {
	return &WxTokenMgr{
		Config:  c,
		session: make(map[string]atomic.Value),
	}
}

func (mgr *WxTokenMgr) GetConfig() Config {
	return mgr.Config
}

func (mgr *WxTokenMgr) GetOrNewToken() (token string, isNew bool, err error) {
	token = mgr.Get(KeyToken)
	if token != "" {
		return
	}

	newToken, err := fetchToken(mgr.AppId, mgr.AppSecret, AppPlatformWxa)
	if err != nil {
		return
	}
	isNew = true
	token = newToken.Token
	err = mgr.Set(KeyToken, newToken.Token, newToken.Expires)
	return
}

func (mgr *WxTokenMgr) Set(k, v string, expire int64) error {
	if expire == 0 {
		expire = 99999999999
	}
	if s, ok := mgr.session[k]; ok {
		s.Store(&Token{
			Token:   v,
			Expires: time.Now().Unix() + expire,
		})
	} else {
		var s atomic.Value
		s.Store(&Token{
			Token:   v,
			Expires: time.Now().Unix() + expire,
		})
		mgr.session[k] = s
	}
	return nil
}

func (mgr *WxTokenMgr) Get(k string) string {
	if token, ok := mgr.session[KeyToken]; ok {
		token := token.Load()
		if token != nil {
			token := token.(*Token)
			if token.IsNotExpire() {
				return token.Token
			}
		}
	}

	return ""
}

//------------------------------------------------------------------------------

// RTokenMgr 基于 go-redis 维护access_token
type RTokenMgr struct {
	Config
	session *redis.Client
}

func NewRTokenMgrByConfig(c Config, session *redis.Client) *RTokenMgr {
	if c.Platform == "" {
		c.Platform = AppPlatformWxa
	}
	return &RTokenMgr{
		Config:  c,
		session: session,
	}
}

func (mgr *RTokenMgr) GetConfig() Config {
	return mgr.Config
}

func (mgr *RTokenMgr) GetOrNewToken() (token string, isNew bool, err error) {
	tokenKey := fmt.Sprintf("%s:%s", KeyToken, mgr.AppId)
	token = mgr.session.Get(context.Background(), tokenKey).Val()
	if token != "" {
		return
	}

	newToken, err := fetchToken(mgr.AppId, mgr.AppSecret, mgr.Platform)
	if err != nil {
		return
	}
	// redis 就依赖expires
	isNew = true
	token = newToken.Token
	err = mgr.session.Set(context.Background(), tokenKey, newToken.Token, time.Duration(newToken.Expires)*time.Second).Err()
	return
}

func (mgr *RTokenMgr) Set(k, v string, expire int64) error {
	tokenKey := fmt.Sprintf("%s:%s", k, mgr.AppId)
	return mgr.session.Set(context.Background(), tokenKey, v, time.Duration(expire)*time.Second).Err()
}

func (mgr *RTokenMgr) Get(k string) string {
	tokenKey := fmt.Sprintf("%s:%s", k, mgr.AppId)
	return mgr.session.Get(context.Background(), tokenKey).Val()
}

//------------------------------------------------------------------------------

// ThirdToken 适配微信第三方平台
type ThirdToken struct {
	Config
	session      *redis.Client
	RefreshToken func() (Token, error)
}

func NewThirdToken(config Config, session *redis.Client, refresh func() (Token, error)) *ThirdToken {
	return &ThirdToken{
		Config:       config,
		session:      session,
		RefreshToken: refresh,
	}
}

func (mgr *ThirdToken) GetConfig() Config {
	return mgr.Config
}

func (mgr *ThirdToken) Set(k, v string, expire int64) error {
	tokenKey := fmt.Sprintf("third:%s:%s", k, mgr.AppId)
	return mgr.session.Set(context.Background(), tokenKey, v, time.Duration(expire)*time.Second).Err()
}

func (mgr *ThirdToken) Get(k string) string {
	tokenKey := fmt.Sprintf("third:%s:%s", k, mgr.AppId)
	return mgr.session.Get(context.Background(), tokenKey).Val()
}

func (mgr *ThirdToken) GetOrNewToken() (token string, isNew bool, err error) {
	tokenKey := fmt.Sprintf("third:%s:%s", KeyToken, mgr.AppId)
	token = mgr.session.Get(context.Background(), tokenKey).Val()
	if token != "" {
		return
	}

	newToken, err := mgr.RefreshToken()
	if err != nil {
		return
	}
	// redis 就依赖expires
	isNew = true
	token = newToken.Token
	err = mgr.session.Set(context.Background(), tokenKey, newToken.Token, time.Duration(newToken.Expires)*time.Second).Err()
	return
}
