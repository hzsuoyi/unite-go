package unite

import (
	"fmt"
)

type (
	AppPlatform string

	Config struct {
		Platform      AppPlatform `yaml:"platform" json:"platform" db:"platform"`
		Name          string      `yaml:"name" json:"name" db:"name"`                          // 昵称
		Username      string      `yaml:"username" json:"username" db:"username"`              // 微信平台 gh_ 开头的
		AppId         string      `yaml:"appId" json:"appId" db:"app_id"`                      // app_id
		AppSecret     string      `yaml:"appSecret" json:"appSecret" db:"app_secret"`          // app_secret
		PublicKey     string      `yaml:"publicKey" json:"publicKey" db:"public_key"`          // 支付宝公钥
		PrivateKey    string      `yaml:"privateKey" json:"privateKey" db:"private_key"`       // 支付宝私钥
		Token         string      `yaml:"token" json:"token" db:"token"`                       // 客服消息用
		AesKey        string      `yaml:"aesKey" json:"aesKey" db:"aes_key"`                   // 客服消息用
		RefreshToken  string      `yaml:"refreshToken" json:"refreshToken" db:"refresh_token"` // 第三方平台用于获取token
		BaiduClientId string      `yaml:"clientId" json:"clientId" db:"baidu_client_id"`       // 百度请求接口时用的AppId
	}

	CommonClient interface {
		TokenMgr
		// 临时素材
		UploadMedia(mediaTyp MediaTyp, media []byte) (mediaId string, err error)
		UploadMediaFile(mediaTyp MediaTyp, filePath string) (mediaId string, err error)
		UploadMediaURL(mediaTyp MediaTyp, filePath string) (mediaId string, err error)
		GetMedia(mediaId string) (media []byte, err error)
		// 客服消息
		SetContactHook(hook ContactHook)
		SendMessage(body SendMessage, extra interface{}) (err error)
		TransferCustomerService(msg ReceiveMessage) RespMessage
		// 客服消息-链接
		NewLink(msg ReceiveMessage, title, desc, url, thumbUrl string) SendMessage
		// 客服消息-文本
		NewText(msg ReceiveMessage, text string) SendMessage
		PostText(msg ReceiveMessage, text string) (SendMessage, error)
		// 客服消息-图片
		NewImage(msg ReceiveMessage, mediaId string) SendMessage
		PostImage(msg ReceiveMessage, mediaId string) (SendMessage, error)
		// 客服消息-小程序
		NewMiniProgramPage(msg ReceiveMessage, title, appId, pagePath, thumbMediaId string) SendMessage
		PostMiniProgramPage(msg ReceiveMessage, title, appId, pagePath, thumbMediaId string) (SendMessage, error)
		// 数据
		GetAnalysisData(action, begin, end string) (rb []byte, err error)
		// 清空API限制
		ClearAPILimit(appId string) (err error)
	}

	Client struct {
		TokenMgr
		contactHook ContactHook
	}

	ClientURL struct {
		WX     string
		TT     string
		Baidu  string
		Worker string
	}
)

const (
	AppPlatformMp         AppPlatform = "微信公众号"
	AppPlatformWxa        AppPlatform = "微信小程序"
	AppPlatformOpenWechat AppPlatform = "微信开放平台"
	AppPlatformChannel    AppPlatform = "微信视频号"
	AppPlatformTT         AppPlatform = "头条小程序"
	AppPlatformBaidu      AppPlatform = "百度小程序"
	AppPlatformAlipay     AppPlatform = "支付宝小程序"
	AppPlatformWorker     AppPlatform = "企业微信"
)

func (c Config) GetAppId() string {
	if c.Platform == AppPlatformBaidu {
		return c.BaiduClientId
	} else {
		return c.AppId
	}
}

func (p AppPlatform) IsMp() bool {
	return p == AppPlatformMp
}

// 是否是微信系列
func (p AppPlatform) IsWechat() bool {
	return p == AppPlatformWxa || p == AppPlatformMp
}

// IsUni 是否是小程序
func (p AppPlatform) IsUni() bool {
	return p != AppPlatformMp
}

// IsWorker 是否是企业微信
func (p AppPlatform) IsWorker() bool {
	return p == AppPlatformWorker
}

func (u *ClientURL) Format(platform AppPlatform, args ...interface{}) string {
	var uu string
	switch platform {
	case AppPlatformTT:
		uu = u.TT
	case AppPlatformBaidu:
		uu = u.Baidu
	case AppPlatformWorker:
		uu = u.Worker
	case AppPlatformAlipay:
		return ""
	default:
		uu = u.WX
	}
	return fmt.Sprintf(uu, args...)
}
