package unite

type (
	SubscribeMessage struct {
		ToUser      string                           `json:"touser"`
		TemplateID  string                           `json:"template_id"`
		Page        string                           `json:"page"` // 跳转网页时填写
		MiniProgram *WXAppPath                       `json:"miniprogram"`
		Data        map[string]SubscribeMessageFiled `json:"data"`
	}

	SubscribeMessageFiled struct {
		Value string `json:"value"`
	}
)
