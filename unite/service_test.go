package unite

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"testing"
)

type LangResp struct {
	Result string `json:"result"`
}

func TestClient_InvokeService(t *testing.T) {
	r := redis.NewClient(&redis.Options{Addr: ":6379"})
	client := &Client{
		TokenMgr: NewRTokenMgrByConfig(Config{
			AppId:     "wx7a0bd6db5f8f4f30",
			AppSecret: "373aee1d232293cf2572e251fccd290f",
		}, r),
	}
	var resp LangResp
	err := client.InvokeService("wxf5c22ebbe4ed811e", "multilingualMT", map[string]string{
		"q":        "我需要测试的语句",
		"toLang":   "ja",
		"fromLang": "zh_CN",
	}, &resp)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(resp.Result)
}
