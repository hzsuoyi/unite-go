package unite

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

const clearAPILimitURL = "https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=%s"

// ClearAPILimit https://developers.weixin.qq.com/doc/offiaccount/Message_Management/API_Call_Limits.html
func (c *Client) ClearAPILimit(appId string) (err error) {
	u := fmt.Sprintf(clearAPILimitURL, c.GetToken())
	data, _ := json.Marshal(struct {
		AppId string `json:"appid"`
	}{AppId: appId})
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	err = uutil.DoRequestJson(req, nil)
	return
}
