package unite

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	jsoniter "github.com/json-iterator/go"
	"net/http"
)

type InvokeServiceRequest struct {
	Service     string      `json:"service"`
	Api         string      `json:"api"`
	Data        interface{} `json:"data"`
	ClientMsgId string      `json:"client_msg_id"`
}

type InvokeServiceResponse struct {
	uutil.ErrResp
	RequestId string `json:"request_id"`
	Data      string `json:"data"`
}

// InvokeService 微信开放平台
// https://developers.weixin.qq.com/doc/oplatform/service_market/buyer_guideline/API/invokeService.html
func (c *Client) InvokeService(id, name string, data, respData interface{}) error {
	body, _ := jsoniter.Marshal(InvokeServiceRequest{
		Service:     id,
		Api:         name,
		Data:        data,
		ClientMsgId: uutil.GetRndString(10),
	})
	u := fmt.Sprintf("https://api.weixin.qq.com/wxa/servicemarket?access_token=%v", c.GetToken())
	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader(body))
	var resp = new(InvokeServiceResponse)
	if err := uutil.DoRequestJson(req, resp); err != nil {
		return err
	}
	return jsoniter.UnmarshalFromString(resp.Data, respData)
}
