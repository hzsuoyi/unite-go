package unite

const (
	LangZhCN = "zh_CN"
	LangZhTW = "zh_TW"
	LangEn   = "en"
	LangJa   = "ja"
	LangKo   = "ko"
)
