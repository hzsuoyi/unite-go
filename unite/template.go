package unite

import (
	"regexp"
	"strings"
)

type (
	Template struct {
		ToUser     string `json:"touser"`
		TemplateId string `json:"template_id"`
		Title      string `json:"title"`
		Content    string `json:"content"` // 微信接口返回的原始模板content
		Example    string `json:"example"`

		Data      TemplateData `json:"data"`       // 模板列表存解析后的模板数据，发送消息时存消息内容
		DataIndex []string     `json:"data_index"` // 保存Data.key的顺序
	}

	TemplateData map[string]TemplateField

	TemplateField struct {
		Value string `json:"value"`
		Color string `json:"color"`
	}

	// TemplateList 模版列表
	TemplateList struct {
		TemplateList []Template `json:"template_list"`
	}

	// MpTemplate 公众号发送模板消息
	MpTemplate struct {
		Template
		Url         string     `json:"url"`
		MiniProgram *WXAppPath `json:"miniprogram,omitempty"`
	}

	// WxaTemplate 小程序端发送模板消息
	WxaTemplate struct {
		Template
		Page            string `json:"page"`
		FormId          string `json:"form_id"`
		EmphasisKeyword string `json:"emphasis_keyword"` // 放大关键词
	}
)

// Set 给模板消息加内容
func (t TemplateData) Set(k, v, color string) {
	t[k] = TemplateField{
		v, color,
	}
}

// 匹配模板消息中 双括号
var doubleBracketsReg, _ = regexp.Compile("\\{\\{(.+?)\\}\\}")

// 解析content
func (t *Template) ParseData() {
	lines := strings.Split(t.Content, "\n")
	if len(lines) == 0 {
		return
	}
	if t.Data == nil {
		t.Data = make(TemplateData)
	}

	for _, line := range lines {
		if len(line) == 0 { // 空行
			continue
		}

		var key, value string
		c := strings.Split(line, "：") // 中文分割符
		for _, val := range c {
			if m := doubleBracketsReg.MatchString(val); m {
				key = strings.TrimSuffix(doubleBracketsReg.FindStringSubmatch(val)[1], ".DATA")
			} else {
				value = strings.TrimSpace(val)
			}
		}
		t.Data.Set(key, value, "")
		t.DataIndex = append(t.DataIndex, key)
	}
}
