package mp

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

const (
	MaterialTypeNews  = "news"
	MaterialTypeImage = "image"
	MaterialTypeVoice = "voice"
	MaterialTypeVideo = "video"
)

const batchGetMaterialURL = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=%s"
const materialAddNewsURL = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=%s"

var ErrInvalidMaterialOffset = errors.New("offset must be non-negative")
var ErrInvalidMaterialCount = errors.New("count must be in the range of [1, 20]")

type (
	MaterialInfo struct {
		uutil.ErrResp

		TotalCount int                `json:"total_count"`
		ItemCount  int                `json:"item_count"`
		Item       []MaterialInfoItem `json:"item"`
	}

	MaterialInfoItem struct {
		MediaId    string `json:"media_id"`
		UpdateTime string `json:"update_time"`

		// 下面专门针对图片、语音和视频
		Name string `json:"name"`
		Url  string `json:"url"`

		// 下面专门针对图文消息
		Content MaterialInfoNewsContent `json:"content"`
	}

	// 图文素材内容
	MaterialInfoNewsContent struct {
		NewsItem MaterialInfoNewsItem `json:"news_item"`
	}

	// 图文素材项
	MaterialInfoNewsItem struct {
		Title            string `json:"title"`
		ThumbMediaId     string `json:"thumb_media_id"`
		ShowCoverPic     string `json:"show_cover_pic"`
		Author           string `json:"author"`
		Digest           string `json:"digest"`
		Content          string `json:"content"`            // HTML本体
		Url              string `json:"url"`                // 文章链接
		ContentSourceUrl string `json:"content_source_url"` // 原文链接
	}

	// 新增永久图文素材的请求内容
	MaterialAddNewsRequest struct {
		Articles []MaterialAddNewsRequestArticle `json:"articles"`
	}

	MaterialAddNewsRequestArticle struct {
		Title              string `json:"title"`
		ThumbMediaId       string `json:"thumb_media_id"`
		Author             string `json:"author"`
		Digest             string `json:"digest"`
		ShowCoverPic       string `json:"show_cover_pic"`
		Content            string `json:"content"`
		ContentSourceUrl   string `json:"content_source_url"`
		NeedOpenComment    int    `json:"need_open_comment"`     // 1=需要开启评论, 0=不开启
		OnlyFansCanComment int    `json:"only_fans_can_comment"` // 1=只允许粉丝留言, 0=随便
	}

	MaterialAddNewsResponse struct {
		uutil.ErrResp
		MediaId string `json:"media_id"`
	}

	batchGetMaterialRequest struct {
		Typ    string `json:"type"`
		Offset int    `json:"offset"`
		Count  int    `json:"count"`
	}
)

// 批量获取最近的素材列表
// https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/Get_materials_list.html
func (client *Client) BatchGetMaterial(typ string, offset int, count int) (lst []MaterialInfo, err error) {
	if offset < 0 {
		return nil, ErrInvalidMaterialOffset
	}

	if count < 1 || count > 20 {
		return nil, ErrInvalidMaterialCount
	}

	b, err := json.Marshal(batchGetMaterialRequest{
		Typ:    typ,
		Offset: offset,
		Count:  count,
	})
	if err != nil {
		return
	}

	u := fmt.Sprintf(batchGetMaterialURL, client.GetToken())
	req, _ := http.NewRequest("POST", u, bytes.NewReader(b))
	err = uutil.DoRequestJson(req, &lst)
	return
}

// 新增一组图文消息
// https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/Adding_Permanent_Assets.html
func (client *Client) MaterialAddNews(articles []MaterialAddNewsRequestArticle) (mediaId string, err error) {
	b, err := json.Marshal(MaterialAddNewsRequest{
		Articles: articles,
	})
	if err != nil {
		return
	}

	var r MaterialAddNewsResponse

	u := fmt.Sprintf(materialAddNewsURL, client.GetToken())
	req, _ := http.NewRequest("POST", u, bytes.NewReader(b))
	err = uutil.DoRequestJson(req, &r)
	mediaId = r.MediaId
	return
}
