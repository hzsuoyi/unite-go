package mp

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

// 发送消息相关内容

// 群发消息的链接
const messageMassSendURL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=%s"

type (
	// 发送消息的结果，支持图文消息、图片、视频、音频等
	SendMessageResponse struct {
		Type      string `json:"type"`
		MediaId   string `json:"media_id"`
		CreatedAt int64  `json:"created_at"`
	}
)

// 群发一组图文消息，tagId=0表示完全群发，否则tagId表示tag标签序号
func (client *Client) SendMessageNews(mediaId string, tagId int) (resp SendMessageResponse, err error) {
	b, err := json.Marshal(struct {
		Filter struct {
			IsToAll bool `json:"is_to_all"`
			TagId   int  `json:"tag_id"`
		} `json:"filter"`
		MpNews struct {
			MediaId string `json:"media_id"`
		} `json:"mpnews"`
		SendIgnoreReprint int `json:"send_ignore_reprint"` // 1=如果被判定为转载继续转发；0=此情况下不继续转发
	}{
		Filter: struct {
			IsToAll bool `json:"is_to_all"`
			TagId   int  `json:"tag_id"`
		}{
			IsToAll: tagId == 0,
			TagId:   tagId,
		},
		MpNews: struct {
			MediaId string `json:"media_id"`
		}{
			MediaId: mediaId,
		},
		SendIgnoreReprint: 0,
	})
	if err != nil {
		return
	}

	u := fmt.Sprintf("%s%s", messageMassSendURL, client.GetToken())
	req, _ := http.NewRequest("POST", u, bytes.NewReader(b))
	var r struct {
		uutil.ErrResp
		SendMessageResponse
	}
	err = uutil.DoRequestJson(req, &r)
	resp = r.SendMessageResponse
	return
}
