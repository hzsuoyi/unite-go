package mp

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	jsoniter "github.com/json-iterator/go"
	"net/http"
)

var ErrTagNotFound = errors.New("tag not found")

const userInfoURL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=%s"
const userInfoBatchURL = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=%s"

// 用户信息
type UserInfo struct {
	uutil.ErrResp
	Subscribe      int64   `json:"subscribe"`
	OpenId         string  `json:"openid"`
	Nickname       string  `json:"nickname"`
	Sex            int64   `json:"sex"`
	Language       string  `json:"language"`
	City           string  `json:"city"`
	Province       string  `json:"province"`
	Country        string  `json:"country"`
	HeadImgUrl     string  `json:"headimgurl"`
	SubscribeTime  int64   `json:"subscribe_time"`
	UnionId        string  `json:"unionid,omitempty"`
	Remark         string  `json:"remark"`
	GroupId        int64   `json:"groupid"`
	TagIdList      []int64 `json:"tagid_list"`
	SubscribeScene string  `json:"subscribe_scene"`
	QrScene        int64   `json:"qr_scene"`
	QrSceneStr     string  `json:"qr_scene_str"`
}

type UserInfoList struct {
	List []UserInfo `json:"user_info_list"`
}

// 获取用户信息
func (client *Client) GetUserInfo(openId, lang string) (userInfo UserInfo, err error) {
	u := fmt.Sprintf(userInfoURL, client.GetToken(), openId, lang)

	req, _ := http.NewRequest(http.MethodGet, u, nil)
	err = uutil.DoRequestJson(req, &userInfo)
	return
}

func (client *Client) GetUserInfoBatch(openId ...string) (userInfo UserInfoList, err error) {
	u := fmt.Sprintf(userInfoBatchURL, client.GetToken())

	var list []map[string]string
	for _, openid := range openId {
		if len(openid) == 0 {
			continue
		}
		list = append(list, map[string]string{
			"openid": openid,
			"lang":   "zh_CN",
		})
	}
	body, _ := jsoniter.Marshal(map[string]interface{}{
		"user_list": list,
	})

	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader(body))
	err = uutil.DoRequestJson(req, &userInfo)
	return
}

//------------------------------------------------------------------------------

const createTagURL = "https://api.weixin.qq.com/cgi-bin/tags/create?access_token=%s"
const removeTagURL = "https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=%s"
const getTagURL = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token=%s"
const getTagUserURL = "https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=%s"
const batchTaggingURL = "https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=%s"
const batchUnTaggingURL = "https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=%s"

type UserTag struct {
	Id    int64  `json:"id,omitempty"`
	Name  string `json:"name"`
	Count int64  `json:"count,omitempty"`
}

// 存储tag下用户详情的上下文
type tagUserFetchResult struct {
	hasNext    bool    // 是否还有下一位
	Client     *Client `json:"client"` // 具体的公众号client
	TagId      int64   `json:"tagId"`
	NextOpenId string  `json:"nextOpenId"`
}

func (client *Client) GetOrCreateTagId(name string) (tagId int64, err error) {
	tags, _ := client.GetTag()
	for _, t := range tags {
		if t.Name == name {
			return t.Id, nil
		}
	}
	if tag, err := client.CreateTag(name); err != nil {
		return 0, err
	} else {
		return tag.Id, nil
	}
}

// 如果找不到tag或者找寻过程出现问题，则返回非nil的error
func (client *Client) GetTagByName(name string) (tag UserTag, err error) {
	if tags, err := client.GetTag(); err != nil {
		return UserTag{}, err
	} else {
		for i := 0; i < len(tags); i++ {
			if tags[i].Name == name {
				return tags[i], nil
			}
		}
		return UserTag{}, ErrTagNotFound
	}
}

// 给定标签的id，尝试删除
func (client *Client) RemoveTag(tagId int64) (err error) {
	u := fmt.Sprintf(removeTagURL, client.GetToken())

	data, _ := json.Marshal(struct {
		Tag struct {
			Id int64 `json:"id"`
		} `json:"tag"`
	}{
		Tag: struct {
			Id int64 `json:"id"`
		}{
			Id: tagId,
		},
	})

	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	return uutil.DoRequestJson(req, nil)
}

// 给定上下文信息，尝试获取指定tag的
func (client *Client) GetTagUserList(ctx *tagUserFetchResult) (openIds []string, err error) {
	if ctx == nil {
		return nil, errors.New("nil argument")
	}
	u := fmt.Sprintf(getTagUserURL, client.GetToken())

	data, _ := json.Marshal(struct {
		TagId      int64  `json:"tagid"`
		NextOpenId string `json:"next_openid"`
	}{
		TagId:      ctx.TagId,
		NextOpenId: ctx.NextOpenId,
	})

	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	resp := struct {
		Count      int    `json:"count"`
		NextOpenId string `json:"next_openid"`
		Data       struct {
			OpenIds []string `json:"openid"`
		} `json:"data"`
	}{}

	if err = uutil.DoRequestJson(req, &resp); err == nil {
		if resp.NextOpenId != "" {
			ctx.NextOpenId = resp.NextOpenId
		}
		return resp.Data.OpenIds, nil
	} else {
		return nil, err
	}
}

// 创建标签
func (client *Client) CreateTag(tag string) (newTag UserTag, err error) {
	u := fmt.Sprintf(createTagURL, client.GetToken())

	data, _ := json.Marshal(struct {
		Tag UserTag `json:"tag"`
	}{
		Tag: UserTag{
			Name: tag,
		},
	})
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))
	resp := struct {
		Tag UserTag `json:"tag"`
	}{}
	err = uutil.DoRequestJson(req, &resp)
	return resp.Tag, err
}

// 获取标签列表
func (client *Client) GetTag() (tags []UserTag, err error) {
	u := fmt.Sprintf(getTagURL, client.GetToken())
	req, _ := http.NewRequest("GET", u, nil)

	var tagList struct {
		Tags []UserTag `json:"tags"`
	}
	if err = uutil.DoRequestJson(req, &tagList); err != nil {
		return
	}
	tags = tagList.Tags
	return
}

// 批量打标签，每次不能超过50个
func (client *Client) BatchTagging(openIdList []string, tagId int64) (err error) {
	if len(openIdList) > 50 {
		return errors.New("openId count must leq 50")
	} else if len(openIdList) == 0 {
		return nil
	}
	u := fmt.Sprintf(batchTaggingURL, client.GetToken())

	data, _ := json.Marshal(struct {
		OpenIdList []string `json:"openid_list"`
		TagId      int64    `json:"tagid"`
	}{
		OpenIdList: openIdList,
		TagId:      tagId,
	})
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	return uutil.DoRequestJson(req, nil)
}

// 批量取消标签
func (client *Client) BatchUnTagging(openIdList []string, tagId int64) (err error) {
	if len(openIdList) > 50 {
		return errors.New("openId count must leq 50")
	} else if len(openIdList) == 0 {
		return nil
	}

	u := fmt.Sprintf(batchUnTaggingURL, client.GetToken())

	data, _ := json.Marshal(struct {
		OpenIdList []string `json:"openid_list"`
		TagId      int64    `json:"tagid"`
	}{
		OpenIdList: openIdList,
		TagId:      tagId,
	})
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	return uutil.DoRequestJson(req, nil)
}

// 给定tagId和下一用户顺位，尝试获取一定数量的用户openId
func (client *Client) FetchTagUserList(tagId int64, nextOpenId string) tagUserFetchResult {
	return tagUserFetchResult{
		hasNext:    true,
		Client:     client,
		TagId:      tagId,
		NextOpenId: nextOpenId,
	}
}

// -------- 定义 tagUserFetchResult 的函数们
func (ctx *tagUserFetchResult) Next() bool {
	return ctx.hasNext
}

func (ctx *tagUserFetchResult) Get() (openIdList []string, err error) {
	openIdList, err = ctx.Client.GetTagUserList(ctx)
	if err != nil || len(openIdList) == 0 {
		ctx.hasNext = false
	}
	return
}
