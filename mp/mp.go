package mp

import (
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"github.com/json-iterator/go"
	"net/http"
)

// 禁止转义html
var json = jsoniter.ConfigFastest

//------------------------------------------------------------------------------

type Client struct {
	*unite.Client
}

// 初始化sdk
func NewClient(client *unite.Client) *Client {
	return &Client{
		Client: client,
	}
}

// 获取jsApi ticket，用于公众号网页jssdk的
func (client *Client) GetJsApiTicket() string {
	token := client.Get(unite.KeyJSApi)
	if token != "" {
		return token
	}

	ticketURL := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi", client.GetToken())
	req, _ := http.NewRequest("GET", ticketURL, nil)

	r := new(struct {
		uutil.ErrResp
		Ticket    string `json:"ticket"`
		ExpiresIn int64  `json:"expires_in"`
	})
	if err := uutil.DoRequestJson(req, r); err != nil {
		return ""
	}

	_ = client.Set(unite.KeyJSApi, r.Ticket, r.ExpiresIn-600)
	//fmt.Printf("wx-auth mp jsApiTicket, appId=%s, token=%s\n", client.GetConfig().AppId, r.Ticket)
	return r.Ticket
}

func (client *Client) GetJsCardTicket() string {
	token := client.Get(unite.KeyJsCard)
	if token != "" {
		return token
	}

	ticketURL := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=wx_card", client.GetToken())
	req, _ := http.NewRequest("GET", ticketURL, nil)

	r := new(struct {
		uutil.ErrResp
		Ticket    string `json:"ticket"`
		ExpiresIn int64  `json:"expires_in"`
	})
	if err := uutil.DoRequestJson(req, r); err != nil {
		return ""
	}

	_ = client.Set(unite.KeyJsCard, r.Ticket, r.ExpiresIn-600)
	//fmt.Printf("wx-auth mp jsCardTicket, appId=%s, token=%s\n", client.GetConfig().AppId, r.Ticket)
	return r.Ticket
}
