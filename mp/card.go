package mp

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"image"
	"io"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"sort"
	"strings"
	"time"
)

type UploadMediaResponse struct {
	uutil.ErrResp
	Url string `json:"url"`
}

// 上传卡券素材
func (client *Client) UploadCardMedia(media []byte) (mediaURL string, err error) {
	b := new(bytes.Buffer)
	writer := multipart.NewWriter(b)

	_, ext, _ := image.Decode(bytes.NewReader(media))
	filename := fmt.Sprintf("%s.%s", uutil.Md5(string(media)), ext)
	var contentTyp string
	if ext == "jpeg" {
		contentTyp = "image/jpeg"
	} else if ext == "png" {
		contentTyp = "image/png"
	}

	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition", fmt.Sprintf(`form-data; name="buffer"; filename="%s"`, filename))
	h.Set("Content-Type", contentTyp)
	part, _ := writer.CreatePart(h)
	part.Write(media)

	writer.Close()

	u := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, u, b)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	respJson := new(UploadMediaResponse)
	if err = uutil.DoRequestJson(req, respJson); err != nil {
		return
	}
	mediaURL = respJson.Url
	return
}

// 使用openid设置白名单
func (client *Client) CardWhitelist(user []string) (err error) {
	u := fmt.Sprintf("https://api.weixin.qq.com/card/testwhitelist/set?access_token=%s", client.GetToken())
	body, _ := json.Marshal(map[string][]string{
		"openid": user,
	})
	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader(body))
	return uutil.DoRequestJson(req, nil)
}

type CardAddResponse struct {
	uutil.ErrResp
	CardId string `json:"card_id"`
}

// 新增卡券
func (client *Client) CardAdd(content string) (cardId string, err error) {
	u := fmt.Sprintf("https://api.weixin.qq.com/card/create?access_token=%s", client.GetToken())
	req, _ := http.NewRequest(http.MethodPost, u, bytes.NewReader([]byte(content)))

	var respJson = new(CardAddResponse)
	if err = uutil.DoRequestJson(req, respJson); err != nil {
		return
	}
	cardId = respJson.CardId
	return
}

// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#54
type JsCardExt struct {
	Code      string `json:"code,omitempty"`
	OpenId    string `json:"openid,omitempty"`
	Timestamp string `json:"timestamp"`
	NonceStr  string `json:"nonce_str"`
	OuterStr  string `json:"outer_str,omitempty"`
	Signature string `json:"signature"`
}

func (client *Client) NewJsCardExt(cardId, code, openId, outerStr string) string {
	// 生成一个随机数
	ext := JsCardExt{
		Code:      code,
		OpenId:    openId,
		Timestamp: fmt.Sprint(time.Now().Unix()),
		NonceStr:  uutil.GetRndString(16),
		OuterStr:  outerStr,
	}
	ext.Signature = signCardExt(client.GetJsCardTicket(), cardId, ext.Code, ext.OpenId, ext.Timestamp, ext.NonceStr, ext.OuterStr)
	s, _ := json.MarshalToString(ext)
	return s
}

func signCardExt(args ...string) string {
	sort.Strings(args)
	h := sha1.New()
	io.WriteString(h, strings.Join(args, ""))
	return fmt.Sprintf("%x", h.Sum(nil))
}
