package mp

import (
	"fmt"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
	"net/url"
)

type (
	OauthParam struct {
		Code  string `form:"code"`
		State string `form:"state"`
		AppId string `form:"appid"`
	}

	OauthToken struct {
		uutil.ErrResp
		AccessToken    string `json:"access_token"`
		ExpiresIn      int64  `json:"expires_in"`
		RefreshToken   string `json:"refresh_token"`
		OpenId         string `json:"openid"`
		Scope          string `json:"scope"`
		IsSnapshotUser int    `json:"is_snapshotuser"`
		UnionId        string `json:"unionid"`
	}

	OauthUserInfo struct {
		uutil.ErrResp
		OpenId     string   `json:"openid"`
		Nickname   string   `json:"nickname"`
		Sex        int64    `json:"sex"`
		Province   string   `json:"province"`
		Country    string   `json:"country"`
		HeadImgUrl string   `json:"headimgurl"`
		Privilege  []string `json:"privilege"`
		UnionId    string   `json:"unionid,omitempty"`
	}
)

// RedirectOauth 获取重定向地址
func (client *Client) RedirectOauth(callbackURL, state string) string {
	if state != "" {
		state = fmt.Sprintf("&state=%s", state)
	}
	return fmt.Sprintf("https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo%s#wechat_redirect",
		client.GetConfig().GetAppId(), url.QueryEscape(callbackURL), state, // 这里就在里边做urlEncode了
	)
}

// RedirectOauthThird 第三方平台获取重定向地址
func (client *Client) RedirectOauthThird(callbackURL, state, componentAppId string) string {
	if state != "" {
		state = "&state=" + state
	}
	if componentAppId != "" {
		componentAppId = "&component_appid=" + componentAppId
	}
	return fmt.Sprintf("https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo%s%s#wechat_redirect",
		client.GetConfig().GetAppId(), url.QueryEscape(callbackURL), state, componentAppId, // 这里就在里边做urlEncode了
	)
}

// GetOauthToken 通过 code 换取 access_token
func (client *Client) GetOauthToken(code string) (token OauthToken, err error) {
	c := client.GetConfig()
	u := fmt.Sprintf("https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code",
		c.GetAppId(), c.AppSecret, code)
	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &token)
	return
}

// GetOauthTokenThird 通过 code 换取 access_token，适用于第三方平台
func (client *Client) GetOauthTokenThird(code, componentAppId, componentAccessToken string) (token OauthToken, err error) {
	c := client.GetConfig()
	u := fmt.Sprintf("https://api.weixin.qq.com/sns/oauth2/component/access_token?appid=%s&code=%s&grant_type=authorization_code"+
		"&component_appid=%s&component_access_token=%s",
		c.GetAppId(), code, componentAppId, componentAccessToken)
	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &token)
	return
}

// GetUserInfoByOauth 获取用户具体信息
func (client *Client) GetUserInfoByOauth(accessToken, openId, lang string) (userInfo OauthUserInfo, err error) {
	u := fmt.Sprintf("https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=%s", accessToken, openId, lang)
	req, _ := http.NewRequest("GET", u, nil)
	err = uutil.DoRequestJson(req, &userInfo)
	return
}
