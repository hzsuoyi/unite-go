package mp

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

const (
	// 发送模板消息
	sendTemplateURL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=%s"

	// 获取所有模版列表的微信请求接口
	getAllTemplateURL = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=%s"
)

// https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751277
func (client *Client) SendTemplateMessage(t unite.MpTemplate) error {
	u := fmt.Sprintf(sendTemplateURL, client.GetToken())

	data, _ := json.Marshal(t)
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	return uutil.DoRequestJson(req, nil)
}

// GetTemplateList 获取所有模版
func (client *Client) GetTemplateList() (list unite.TemplateList, err error) {
	url := fmt.Sprintf(getAllTemplateURL, client.GetToken())

	req, _ := http.NewRequest(http.MethodPost, url, nil)
	err = uutil.DoRequestJson(req, &list)

	return
}

func NewTemplateMessage(toUser, templateId, url string, data unite.TemplateData, mini *unite.WXAppPath) unite.MpTemplate {
	return unite.MpTemplate{
		Template: unite.Template{
			ToUser:     toUser,
			TemplateId: templateId,
			Data:       data,
		},
		Url:         url,
		MiniProgram: mini,
	}
}
