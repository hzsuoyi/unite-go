package mp

import (
	"bytes"
	"fmt"
	"gitee.com/hzsuoyi/unite-go/unite"
	"gitee.com/hzsuoyi/unite-go/uutil"
	"net/http"
)

const sendSubscribeMsgUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/bizsend?access_token=%s"

func (client *Client) SendSubscribeMessage(msg unite.SubscribeMessage) error {
	u := fmt.Sprintf(sendSubscribeMsgUrl, client.GetToken())

	data, _ := json.Marshal(msg)
	req, _ := http.NewRequest("POST", u, bytes.NewReader(data))

	return uutil.DoRequestJson(req, nil)
}

func NewSubscribeMessage(toUser, templateId, page string, data map[string]unite.SubscribeMessageFiled, mini *unite.WXAppPath) unite.SubscribeMessage {
	return unite.SubscribeMessage{
		ToUser:      toUser,
		TemplateID:  templateId,
		Data:        data,
		Page:        page,
		MiniProgram: mini,
	}
}
