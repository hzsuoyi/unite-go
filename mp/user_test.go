package mp

import (
	"gitee.com/hzsuoyi/unite-go/unite"
	"testing"
)

var mockClient *Client

// 普通测试：纯粹简单打标签，不考虑极端情况（如数量过多等）
func TestUserTaggingNormal(t *testing.T) {
	tearDown := setup(t)
	defer tearDown(t)

	const tagName = "test_free_001"
	// 获取我们需要的tag
	tag, err := mockClient.GetTagByName(tagName)
	if err != nil {
		if tag, err = mockClient.CreateTag(tagName); err != nil {
			t.Error(err)
		}
	}

	// 向tag增加特定的openId, 英文逗号分割
	openIds := []string{"ogPMu1IhSb2f3YGFcKLhaUSvGruI", "ogPMu1B6l7MCnYfKT1Ub0HEOd0CA"}
	if err = mockClient.BatchTagging(openIds, tag.Id); err != nil {
		t.Errorf("cannot tag user, err=%s", err.Error())
	}

	// 获取tag，确保最多只有2位
	tag, err = mockClient.GetTagByName(tagName)
	if err != nil {
		t.Errorf("cannot get tag, err=%s", err.Error())
	} else if tag.Count < 2 {
		t.Errorf("tag count<2, halt")
	}

	// 获取tag的所有用户，检查是否存在我们需要的用户
	var remoteOpenIds []string
	fetcher := mockClient.FetchTagUserList(tag.Id, "")
	for fetcher.Next() {
		if ids, err := fetcher.Get(); err == nil {
			remoteOpenIds = append(remoteOpenIds, ids...)
		}
	}
	for _, expOpenId := range openIds {
		var found = false
		for _, remoteOpenId := range remoteOpenIds {
			if expOpenId == remoteOpenId {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("cannot find openId=%s in remote list (count=%d)", expOpenId, len(remoteOpenIds))
		}
	}

	// 删除tag！
	if err = mockClient.RemoveTag(tag.Id); err != nil {
		t.Errorf("cannot remove tagId=%d, err=%s", tag.Id, err.Error())
	}

	// 检查tag是否存在
	tag, err = mockClient.GetTagByName(tagName)
	if err == nil {
		t.Errorf("expected tag been removed, but still exists, tagId=%d", tag.Id)
	}
}

// 初始化测试案例的其他内容，顺便返回tearDown函数
func setup(t *testing.T) func(t *testing.T) {
	if mockClient == nil {
		// FIXME: 全部测试输入全部来自文件！191007前改造完毕!
		// reader := bufio.NewReader(os.Stdin)
		// fmt.Println("Please input mp appId:")
		// appId, _ := reader.ReadString('\n')
		//fmt.Println("Please input mp accessToken:")
		//accessToken, _ := reader.ReadString('\n')
		accessToken := "25_gjb_LknE4nhh6sQA64pAH05OnE3x1QUYErU8ZNWUhibv8SffJrRNmnrm6h9PL6DqXQlX8JZzsXyzeRqR6a7AQ6nQ5OGIukHVNhKyF0_rbbl1DWD6R_A3VKiPFxoRCNbAFAITE"

		mockClient = &Client{
			Client: &unite.Client{
				Logger:         &mockLogger{},
				TokenMgr:       unite.NewWxTokenMgr("wx5360fd0501697da0", ""),
				MediaMgr:       nil,
				ContactMgr:     nil,
				SendContactMgr: nil,
			},
		}
		mockClient.TokenMgr.Set(unite.KeyToken, accessToken, 9999999999)
	}

	return func(t *testing.T) {
		// there's really nothing to be done
	}
}

type mockLogger struct{}

func (*mockLogger) Debugf(format string, args ...interface{}) {
}

func (*mockLogger) Infof(format string, args ...interface{}) {
}

func (*mockLogger) Warnf(format string, args ...interface{}) {
}

func (*mockLogger) Errorf(format string, args ...interface{}) {
}

func (*mockLogger) Fatalf(format string, args ...interface{}) {
}

func (*mockLogger) Panicf(format string, args ...interface{}) {
}
